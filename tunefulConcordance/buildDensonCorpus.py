import utils
from os import listdir
from pathlib import Path
from pickle import dump
from sqlite3 import connect
from music21.repeat import ExpanderException

def main():
    corpus = []
    basedir = Path('../shapenote.net/sorted_abc/denson')
    conn = connect('../minutes.db')
    fmt = ".abc"
    for name in [f for f in listdir(basedir) if fmt in f]:
        p = basedir / name
        try:
            tune = utils.buildTuneFromPath(p)
        except (IndexError, ExpanderException) as e:
            print(p, "PARSEERROR:", e)
            tune = None

        if tune:
            query = "SELECT title FROM songs WHERE PageNum=?"
            (tune.title, ) = conn.execute(query, (tune.page(),)).fetchone()
            corpus.append(tune)
            print(tune)

    outfile = "denson.corpus"
    with open(outfile, "bw") as f:
        dump(corpus, f)
    print("Wrote to %s." % outfile)


main()
