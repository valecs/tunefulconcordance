from music21 import note, chord, stream, converter
from copy import deepcopy
from pathlib import Path
from re import search
from collections import defaultdict

def streamHasChord(s):
    return chord.Chord in map(type, s.flat.elements)


def getMeasures(s):
    return [m for m in s.iter if type(m) is stream.Measure]


def dechordMeasure(m):
    high = deepcopy(m)
    low = deepcopy(m)
    for (s, cmp) in [(high, max), (low, min)]:
        for e in s.elements:
            if isinstance(e, note.GeneralNote) and e.isChord:
                s.replace(e, note.Note(cmp(e.pitches), duration=e.duration))
    return (high, low)


# idea: for each voice with chords, find the measures that have them,
# create 2 (upper and lower), chordless voicings and then run
# getDegreeStringFromPart() on each voicing
# list(filter(streamHasChord, getMeasures(l['al'])))
def dechordPart(p):
    high = deepcopy(p)
    low = deepcopy(p)
    for (m, mh, ml) in zip(*(map(getMeasures, [p, high, low]))):
        if streamHasChord(m):
            h, l = dechordMeasure(m)
            high.replace(mh, h)
            low.replace(ml, l)
    return (high, low)


def getDegreeStringFromPart(part, k=None):
    if k is None:  # Assume we want the relative major for solfege purposes
        k = part.analyze('key').getRelativeMajor()
    degrees = []
    if streamHasChord(part):
        raise Warning("You shouldn't decode streams containing chords!")
    for p in part.expandRepeats().pitches:
        degrees.append(k.getScaleDegreeFromPitch(p))
    return ''.join(map(str, degrees))


# a corpus will be a list of tunes
class Tune:
    parts = {}
    pageNumber = None
    loc = None
    variant = None
    title = "Unknown"
    stream = None

    def __lt__(self, other):
        if self.pageNumber and other.pageNumber:
            return self.pageNumber < other.pageNumber
        else:
            return True

    def __repr__(self):
        pn = self.page()
        if self.variant:
            pn += self.variant

        return "<%s, %s>" % (self.title, pn)

    def __init__(self, parts, pageNumber=None, loc=None, variant=None,
                 book=None, title=None, stream=None):
        self.parts = parts
        if pageNumber:
            self.pageNumber = int(pageNumber)
        if loc:
            self.loc = loc
        if variant:
            self.variant = variant
        if book:
            self.book = book
        if title:
            self.title = title
        if stream:
            self.stream = stream

    def page(self):
        if self.pageNumber:
            pn = str(self.pageNumber)
            if self.loc:
                pn += self.loc
        else:
            pn = ""
        return pn



def buildTuneFromPath(path):
    tune = converter.parse(path)
    key = tune.analyze('key').getRelativeMajor()
    parts = {}
    for (i, k) in enumerate(["tr", "al", "te", "ba"]):
        part = tune.parts[i]
        if streamHasChord(part):
            p_high, p_low = dechordPart(part)
            parts[k+"-high"] = getDegreeStringFromPart(p_high, key)
            parts[k+"-low"] = getDegreeStringFromPart(p_low, key)
        else:
            parts[k] = getDegreeStringFromPart(part, key)

    pn = parseDensonPath(path)
    t = Tune(parts, **pn, book="denson", stream=tune)
    return t


def parseDensonPath(path):
    p = Path(path).stem
    # "[0-9]{1,3}[tb]?[dam]?"
    r = {
        'pageNumber': None,
        'loc': None,
        'variant': None,
    }
    for (k, pattern) in {'pageNumber': "[0-9]{1,3}", 'loc': "[tb]",
                         'variant': "[dam]"}.items():
        m = search(pattern, p)
        if m:
            r[k] = m.group()
    return r


# filter(bool, map(matchTune, corpus)))
def matchTune(tune, pattern, translate=None):
    parts = []
    for (part, sequence) in tune.parts.items():
        if translate:
            sequence = ''.join(map(lambda c: translate[c], sequence))
        loc = sequence.find(pattern)
        if loc is not -1:
            parts.append((part, loc + 1))
            # okay our loc scheme is getting messed up by the
            # translation---you can't just count characters to get
            # notes.

        # loc = findAll(sequence, pattern) if loc: parts.append((part,
        # list(loc)))
    if parts:
        return (tune, parts)
    else:
        return None


s4 = defaultdict(
    lambda: '',
    {'1': 'fa',
     '2': 'so',
     '3': 'la',
     '4': 'fa',
     '5': 'so',
     '6': 'la',
     '7': 'mi'
     }
)

s7 = defaultdict(
    lambda: '',
    {'1': 'do',
     '2': 're',
     '3': 'mi',
     '4': 'fa',
     '5': 'so',
     '6': 'la',
     '7': 'si'
     }
)


def matchTuneShapes(tune, pattern, shapeDict):
    parts = []
    for (part, sequence) in tune.parts.items():
        sequence = ''.join(map(lambda c: shapeDict[c], sequence))
        loc = sequence.find(pattern)
        if loc is not -1:
            parts.append((part, int(loc/2) + 1))
        # loc = findAll(sequence, pattern) if loc: parts.append((part,
        # list(loc)))
    if parts:
        return (tune, parts)
    else:
        return None


def searchShapes(query, corpus, shapeDict):
    return filter(
        bool, map(
            lambda t:
            matchTuneShapes(t, query.replace(" ", ""), shapeDict),
            corpus))


def findAll(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1:
            return
        yield start
        start += len(sub)  # use start += 1 to find overlapping matches
