from utils import matchTune, searchShapes, s4
from pickle import load
from collections import defaultdict
from sys import argv

corpusfile = "denson.corpus.bak"
with open(corpusfile, "br") as f:
    corpus = load(f)

# print(list(
#     filter(bool,
#            map(lambda t: matchTune(t, "6555432"), corpus)
#            )))

toDenson = defaultdict(lambda: '',
                       {'1': 'fa',
                        '2': 'so',
                        '3': 'la',
                        '4': 'fa',
                        '5': 'so',
                        '6': 'la',
                        '7': 'mi'
                        }
                       )


query = "6555432"
print(query)
for (tune, match) in filter(bool,
                            map(lambda t: matchTune(t, query), corpus)):
    print(tune, match)

print()

query = "la mi fa so la so fa mi la"
if len(argv) > 1:
    query = ' '.join(argv[1:])

print(query)

for (tune, match) in searchShapes(query, corpus, s4):
    print(tune, match)
