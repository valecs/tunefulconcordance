#!/bin/bash
set -uo pipefail
IFS=$'\n\t'

function getMusic(){
    H=${1%.mus}.htm
    grep -ho '\"[[:space:](),[:alnum:]-]*.mus"' ../$H | \
	tr -d '"' | sort | uniq
}


for C in fragments/*.fragment;
#for C in fragments/misc.fragment
do
    book=$(basename ${C%.*})
    echo $book
    mkdir -p $book
    missing=$book/$book.missing
    # rm $missing 2> /dev/null || true
    for F in $(cat $C | \
		   grep -ho '\"[[:space:](),[:alnum:]-]*.htm"' | \
		   tr -d '"' | sed 's/htm/mus/')
    do
	if ls -l "../$F" &> /dev/null || wget -q -P .. "shapenote.net/$F"
	then
	    cp -t $book "../$F"
	    echo -n .
	else
	    if ls ../$(getMusic "$F") > /dev/null
	    then
		cp -t $book  "../$(getMusic "$F")"
		echo -n .
	    else
		# echo "$F" >> $missing
		true
	    fi
	fi
    done
    echo 
done
