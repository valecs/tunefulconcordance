wget --recursive --base=shapenote.net/music.htm --domains=shapenote.net \
     --show-progress --progress=dot  --no-host-directories \
     --limit-rate=50K shapenote.net/music.htm


grep -Rho '\"[[:space:](),[:alnum:]-]*.mus"' . | sort | uniq | tr -d '"' > musfiles

wget --base=shapenote.net/music.htm --domains=shapenote.net --show-progress --progress=dot  --no-host-directories --limit-rate=50K --input-file=musfiles
